# LEARNING PATHWAYS

These pathways are created with the intent of helping beginners filter through the plethora of online resources for learning programming or related fields. If you have any suggestions or questions, please create an issue from the issues tab.

*Table of Contents*

1. Web
   1. [Front End](WEB_FRONT_END.md)
   1. [Back End](WEB_BACK_END.md)
1. Mobile Development
   1. [Cross-Platform JavaScript](MOBILE_JAVASCRIPT.md)
